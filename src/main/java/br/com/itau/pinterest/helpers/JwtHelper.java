package br.com.itau.pinterest.helpers;

import java.util.Optional;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JwtHelper {

	private static final String chave = "oiadsnvioasdnvweoin";
	
	public static Optional<String> gerar(String email) {
		try {
			Algorithm algoritmo = Algorithm.HMAC256(chave);
			String token = JWT.create()
					.withClaim("emailUsuario", email)
					.sign(algoritmo);
			
			return Optional.of(token);
		} catch (Exception exception) {
			return Optional.empty();
		}
	}
	
	public static Optional<String> verificar(String token){
		token = tratarToken(token);
		
		try {
			Algorithm algoritmo = Algorithm.HMAC256(chave);
			
			JWTVerifier verificador = JWT.require(algoritmo).build();
			DecodedJWT tokenDecodificado = verificador.verify(token);
			
			String email = tokenDecodificado.getClaim("emailUsuario").asString();
			
			return Optional.of(email);
		} catch (Exception e) {
			return Optional.empty();
		}		
	}
	
	private static String tratarToken(String token) {
		token = token.replace("Bearer", "");
		token = token.trim();
		
		return token;
	}
}
