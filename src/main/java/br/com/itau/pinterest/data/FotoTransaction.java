package br.com.itau.pinterest.data;

import br.com.itau.pinterest.models.Foto;
import br.com.itau.pinterest.models.FotoUsuario;

public class FotoTransaction {
	private Foto foto;
	private FotoUsuario fotoUsuario;
	
	public Foto getFoto() {
		return foto;
	}
	public void setFoto(Foto foto) {
		this.foto = foto;
	}
	public FotoUsuario getFotoUsuario() {
		return fotoUsuario;
	}
	public void setFotoUsuario(FotoUsuario fotoUsuario) {
		this.fotoUsuario = fotoUsuario;
	}
}
