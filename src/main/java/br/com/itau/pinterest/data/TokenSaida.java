package br.com.itau.pinterest.data;

public class TokenSaida {
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
