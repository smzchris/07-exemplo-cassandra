package br.com.itau.pinterest.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.pinterest.data.FotoTransaction;
import br.com.itau.pinterest.models.Foto;
import br.com.itau.pinterest.models.FotoUsuario;
import br.com.itau.pinterest.repositories.FotoRepository;
import br.com.itau.pinterest.repositories.FotoUsuarioRepository;

@Service
public class FotoService {
	
	@Autowired
	FotoRepository fotoRepository;
	
	@Autowired
	FotoUsuarioRepository fotoUsuarioRepository;
	
	public FotoTransaction inserir(Foto foto) {
		FotoTransaction fotoTransaction = new FotoTransaction();
		
		fotoTransaction.setFoto(inserirFoto(foto));
		fotoTransaction.setFotoUsuario(inserirFotoUsuario(foto));
		
		return fotoTransaction;
	}
	
	private Foto inserirFoto(Foto foto) {
		foto.setId(UUID.randomUUID());
		foto.setDataPostagem(LocalDateTime.now());
		return fotoRepository.save(foto);
	}
	
	private FotoUsuario inserirFotoUsuario(Foto foto) {
		FotoUsuario fotoUsuario = new FotoUsuario();
		
		fotoUsuario.setEmailUsuario(foto.getEmail());
		fotoUsuario.setDataPostagemFoto(foto.getDataPostagem());
		fotoUsuario.setIdFoto(foto.getId());
		fotoUsuario.setLinkFoto(foto.getLink());
		
		return fotoUsuarioRepository.save(fotoUsuario);
	}
	
	public Iterable<Foto> listarFotos(){
		return fotoRepository.findAll();
	}
}
